require 'elasticsearch/model'
class Product < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  attr_accessible :description, :summary, :title
  validates :title, :presence => true
  validates :description, :presence => true
  
end
